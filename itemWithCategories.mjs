import Item from './item.mjs';
import Mall from './mall.mjs';




const itemOne = new Item("iPhone", 1000,"Electronics", 1);
const itemTwo = new Item("Earring", 50,"Jewelleries", 5);
const itemThree = new Item("Spoon", 30,"Utensils", 11);
const itemFour = new Item("Doll", 45,"KidsToys", 21);
const itemFive = new Item("Pan", 25,"Utensils", 4);
const itemSix = new Item("Necklace", 100,"Jewelleries", 8);
const itemSeven = new Item("Laptop", 1200, "Electronics",32);
const itemEight = new Item("Anklet", 15, "Jewelleries",25);
const itemNine = new Item("Laddle", 32,"Utensils", 6);
const itemTen = new Item("KidCar", 50,"KidsToys", 2);
const itemEleven = new Item("Skirt", 20,"Partywears", 12);
const itemTwelve = new Item("Printer", 60, "Electronics",35);
const itemThirteen = new Item("Gown", 200,"Partywears", 7);
const itemFourteen = new Item("KidBall", 22,"KidsToys", 10);
const itemFifteen = new Item("CropTop", 27, "Partywears",18);

const myMall = new Mall();

const listOfItems = [itemOne, itemTwo, itemThree, itemFour, itemFive, itemSix, itemSeven, itemEight,itemNine, itemTen, itemEleven, itemTwelve, itemThirteen, itemFourteen, itemFifteen];

listOfItems.forEach( item => myMall.inventory.addItem(item));

myMall.sendAllItemsToShop();


myMall.shop.showItems();


class Shop {
    constructor() {
        this.itemToBeDisplayed = [];
    }

    addItem(itemObj) {
        this.itemToBeDisplayed.push(itemObj);
        console.log(`Added item in the shop is ${itemObj.name}`);
    }

    addAllItems(allItems) {
        allItems.forEach( oneitem => this.addItem(oneitem));
    }

    showItems() {
        console.log(`Displaying all the ${this.itemToBeDisplayed.length} items in the list `);
        this.itemToBeDisplayed.forEach(item => console.log(item.name));
    }
}

export default Shop;
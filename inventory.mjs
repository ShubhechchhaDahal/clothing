class Inventory {
    constructor() {
        this.items = [];

    }

    addItem(itemObj) {
        this.items.push(itemObj);
        console.log(`The item ${itemObj.name} has been added to the inventory.`);
    }

    sendItem() {
        return this.items.pop();
    }

    sendAllItems() {
        return this.items;
    }
}

export default Inventory;








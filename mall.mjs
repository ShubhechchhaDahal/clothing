import Inventory from "./inventory.mjs";
import Shop from "./shop.mjs";


class Mall {
    constructor() {
        this.inventory = new Inventory();
        this.shop = new Shop();
    }

    sendItemToShop() {
        let item = this.inventory.sendItem();
        this.shop.addItem(item);
        console.log(`Moved ${item.name} from inventory to the shop`);
    }

    sendAllItemsToShop() {
        let allItems = this.inventory.sendAllItems();
        this.shop.addAllItems(allItems);
    }
}

export default Mall;
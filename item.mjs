class Item {
    constructor(name, price, category, barCodeNum) {
        this.name = name;
        this.price = price;
        this.category = category;
        this.barCodeNum = barCodeNum;
    }
}

export default Item;